using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LettersBaul : MonoBehaviour
{
    public GameObject panelBaul;
    bool canHelp = true;
    public static Animator baulAnim;
    public void AddNewLetter(int letter)
    {
        if (canHelp)
        {
            GameManager.instance.UnlockLeter(letter);
            canHelp = false;
        }

    }

    public void OnMouseDown()
    {
        panelBaul.SetActive(true);
        HistoriaCafeteria.dialogoInteractivo = true;
        //panelBomba.transform.position = bombaStartPoss;
        baulAnim.SetBool("Open", true);

    }

    public void ExitPanel()
    {
        panelBaul.SetActive(false);
        HistoriaCafeteria.dialogoInteractivo = false;
        //panelBomba.transform.position = new Vector3(3000, 3000, 0);
        baulAnim.SetBool("Open",false);

    }
    // Start is called before the first frame update
    void Awake()
    {
        baulAnim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
