using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Bomba : MonoBehaviour
{
    public GameObject panelEmpezar;
    public GameObject panelBomba;
    public InputField iptRespuestaFinal;
    public GameObject panelganaste;
    public GameObject panelperdiste;
    public float timerValue = 300;
    public Text txtTimer, txtRespuesta;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (timerValue > 0)
        {
            timerValue -= Time.deltaTime;
        }
        else
        {
            timerValue = 0;
        }
        DisplayTime(timerValue);

        if (timerValue==0)
        {
            SceneManager.LoadScene("Perdiste");
            panelperdiste.SetActive(true);
            Debug.Log("Perdiste");
        }
    }

    void DisplayTime(float timeToDisplay)
    {
        if (timeToDisplay < 0)
        {
            timeToDisplay = 0;
        }
        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);
        float millisecond = timeToDisplay % 1 * 1000;

        txtTimer.text = string.Format("{0:00}:{1:00}:{2:000}", minutes, seconds, millisecond);
    }
    public void Empezar()
    {
        panelEmpezar.SetActive(false);
        GameManager.instance.gameStarted = true;
    }
    public void AnswerGeneral()
    {
        string newname = iptRespuestaFinal.text;
        if (timerValue >= 0 && newname == "LANDA" || newname == "landa")
        {
            SceneManager.LoadScene("Ganaste");
            panelganaste.SetActive(true);
        }
        else
        {
            txtRespuesta.text = "incorrecto";
        }
    }

  

}
