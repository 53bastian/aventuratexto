using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public bool gameStarted = false;
    public int totalQuestions;
    public int currentQuestions=0;
    public static GameManager instance;
    public InputField iptRespuesta;
    public List<GameObject> letters;
    public Transform continer;
    public GameObject inventoryPanel;
    Vector3 inventoryStartPoss;
    bool inventoryOpen = true;
    public int correctAnswers=0;
    public bool questionPanel= false;
    private void Awake()
    {
        instance=this;
        iptRespuesta.enabled = false;
        //inventoryPanel.SetActive(false);
        inventoryStartPoss = inventoryPanel.transform.position;

    }
    public void UnlockLeter()
    {
        GameObject newLetter1 = Instantiate(letters[Random.Range(0, letters.Count)],continer);
        GameObject newLetter2 = Instantiate(letters[Random.Range(0, letters.Count)],continer);

    }

    public void UnlockLeter(int n)
    {
        GameObject newLetter1 = Instantiate(letters[n], continer);
        

    }
    public void UpdateCurrentQuestions()
    {
        currentQuestions++;
        
    }

    public void UpdateAnsweredQuestions()
    {
        correctAnswers++;
        if (correctAnswers == totalQuestions)
        {
            SceneManager.LoadScene("Ganaste");

        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I) && !questionPanel)
        {
            //inventoryPanel.SetActive(!inventoryPanel.activeSelf);
            inventoryOpen = !inventoryOpen;
            if (inventoryOpen)
            {
                inventoryPanel.transform.position = inventoryStartPoss;
            }
            else
            {
                inventoryPanel.transform.position = new Vector3(3000,3000,0);
            }
        }
    }
}
