﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HistoriaCafeteria : MonoBehaviour
{
    public static bool dialogoInteractivo=false;
    public DialogoCafeteria dialogoSuperviviente2;
    public GameObject Panel;
    public GameObject ButtonIniciarConversacion;
    public GameObject Dialogue;
    public GameObject Titulo;
    public InputField IptRespuesta;
    public GameObject ButtonAcept;
    public GameObject ButtonExit;
    public Text txtRepsuesta;
    public GameObject panelInventario;
    //public GameObject Letra1;
    //public GameObject Letra2;
    //public GameObject Letra3;
    //public GameObject Letra4;
    //public GameObject Letra5;

    void Update() {
        Inventory();
    }
    public void DesencaDialogo() {
        FindObjectOfType<DialogoManagerCafeteria>().StartDialogo(dialogoSuperviviente2);
    }
    public void DesactivarBoton() {
        ButtonIniciarConversacion.SetActive(false);
    }
    public void ActivaBotonFin() {
        Titulo.SetActive(true);
        Dialogue.SetActive(true);
        IptRespuesta.gameObject.SetActive(true);
        ButtonAcept.SetActive(true);
        ButtonExit.SetActive(true);
        txtRepsuesta.gameObject.SetActive(true);
    }

    public void AdivinanzaCafeteria() {
        string newname = IptRespuesta.text;
        if (newname == "Pan" || newname == "pan"){
            if (IptRespuesta.enabled)
            {
                txtRepsuesta.text = "Correcto, ganaste 2 letras";
                //Letra1.SetActive(true);
                IptRespuesta.enabled = false;
                GameManager.instance.UnlockLeter(3);
                GameManager.instance.UpdateCurrentQuestions();
                
            }  
        }
        else
            txtRepsuesta.text = "Incorrecto";
    }

    public void AdivinanzaGranja() {
        string newname = IptRespuesta.text;
        if (newname == "manzana" || newname == "Manzana") {
            if (IptRespuesta.enabled)
            {
                txtRepsuesta.text = "Correcto, ganaste 2 letras";
                //Letra2.SetActive(true);
                IptRespuesta.enabled = false;
                GameManager.instance.UpdateCurrentQuestions();
                GameManager.instance.UnlockLeter(1);
                
            }         
        }
        else 
            txtRepsuesta.text = "Incorrecto";
    }

    public void AdivinanzaSupermercado() {
        string newname = IptRespuesta.text;
        if (newname == "Agua" || newname == "agua"){
            if (IptRespuesta.enabled)
            {
                txtRepsuesta.text = "Correcto, ganaste 2 letras";
                //Letra3.SetActive(true);
                IptRespuesta.enabled = false;
                GameManager.instance.UpdateCurrentQuestions();
                GameManager.instance.UnlockLeter(0);
                
            }    
        }
        else
            txtRepsuesta.text = "Incorrecto";
    }

    public void AdivinanzaFarmacia() {
        string newname = IptRespuesta.text;
        if (newname == "Vida" || newname == "vida"){
            if (IptRespuesta.enabled)
            {
                txtRepsuesta.text = "Correcto, ganaste 2 letras";
                //Letra4.SetActive(true);
                IptRespuesta.enabled = false;
                GameManager.instance.UpdateCurrentQuestions();
                GameManager.instance.UnlockLeter(2);
               
            }        
        }
        else
            txtRepsuesta.text = "Incorrecto";
    }
    public void AdivinanzaRestaurante() {
        string newname = IptRespuesta.text;
        if (newname == "espagueti" || newname == "Espagueti"){
            if (IptRespuesta.enabled)
            {
                txtRepsuesta.text = "Correcto, ganaste 2 letras";
                //Letra5.SetActive(true);
                IptRespuesta.enabled = false;
                GameManager.instance.UpdateCurrentQuestions();
                GameManager.instance.UnlockLeter(4);
               
            }  
        }   
        else
            txtRepsuesta.text = "Incorrecto";
    }

    public void ExitQuestion() {
        Panel.SetActive(false);
        HistoriaCafeteria.dialogoInteractivo = false;
        GameManager.instance.questionPanel = false;
    }

    void Inventory(){
        if (Input.GetKeyDown("r") )
        {
            panelInventario.SetActive(true);
        }
        
    }
}
