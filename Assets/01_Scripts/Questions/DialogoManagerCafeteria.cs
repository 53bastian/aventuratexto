﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogoManagerCafeteria : MonoBehaviour
{
    public Queue<string> sentenciaCafeteria;
    public Text nombretextoSuperviviente2;
    public Text DialogoTextoCafeteria;
    public GameObject Cafeteria;
    public GameObject IniciarConversacion;
    public GameObject FinConversacion;
    //public GameObject ContinuarSuper2;

    // Start is called before the first frame update
    void Start()
    {
        sentenciaCafeteria = new Queue<string>();
    }
    public void StartDialogo(DialogoCafeteria dialogoCafeteria)
    {
        // Debug.Log("Comienza la Conversacion con" + dialogo.nombre);
        nombretextoSuperviviente2.text = dialogoCafeteria.nombreSuperviviente2;
        sentenciaCafeteria.Clear();

        foreach (string sentence in dialogoCafeteria.sentenciaSuperviviente2)
        {
            sentenciaCafeteria.Enqueue(sentence);
        }
        SiguienteOracion();
    }

    public void SiguienteOracion()
    {
        
        if (sentenciaCafeteria.Count == 0)
        {
            EndDialogo();
            return;
        }
        string sentence = sentenciaCafeteria.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));

        //DialogoTexto.text = sentence;
        //Debug.Log(sentence);
    }
    IEnumerator TypeSentence(string sentence)
    {
        DialogoTextoCafeteria.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            DialogoTextoCafeteria.text += letter;
            yield return null;
        }
    }
    void EndDialogo()
    {
        Debug.Log("Termina la conversacion");
        FinConversacion.SetActive(true);
        //ContinuarSuper2.SetActive(false);
        DialogoTextoCafeteria.gameObject.SetActive(false);
        sentenciaCafeteria.Clear();
        
        //IniciarConversacion.SetActive(true);

    }
    public void TerminaArgumento()
    {
        Cafeteria.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
