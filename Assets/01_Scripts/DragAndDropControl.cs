using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DragAndDropControl : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IDropHandler, IPointerClickHandler {
    
    public Image itemImg;
    public Transform startParent;
    public Vector2 startPosition;
    public char letter;
    public bool canMove = true;
    public bool bomb = false;
    public float timer=120f;
    public Text timerText;
    DragAndDropControl slotReference = null;
    public bool avaible= true;
    
    void Start()
    {
        startParent = transform.parent;
        if (canMove)
        {
            int r= Random.Range(0,10);
            if (r<=3)
            {
                bomb = true;
                timerText.gameObject.SetActive(true);
            }
            else
            {
                timerText.gameObject.SetActive(false);
            }
        }
    }
    void Update()
    {
        if (bomb)
        {
            if (timer>0)
            {
                timer -= Time.deltaTime;
                timerText.text = ((int)timer).ToString();
            }
            else
            {
                StartCoroutine(Destroyer());
            }
        }
    }

    IEnumerator Destroyer()
    {
        bomb = true;
        GetComponent<Animator>().SetTrigger("Destroyed");
        yield return new WaitForSeconds(0.2f);
        if (slotReference != null)
        {
            slotReference.avaible = true;
        }
        Destroy(gameObject);
    } 

    public void OnBeginDrag(PointerEventData eventData) {
        if(canMove){
            startPosition = new Vector2(transform.localPosition.x, transform.localPosition.y);
            itemImg.raycastTarget = false;
        }
        Debug.Log(name + " OnBeginDrag");
    }

    public void OnDrag(PointerEventData eventData) {
        if(canMove){
            transform.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            Debug.Log(name + " OnDrag");
        }
    }

    public void OnEndDrag(PointerEventData eventData) {
        if(canMove){
            // Debug.Log(eventData.pointerEnter.name);
            if(eventData.pointerEnter != null){
                DragAndDropControl another = eventData.pointerEnter.GetComponent<DragAndDropControl>();
                
                if(another != null){
                    if(letter == another.letter && another.avaible){
                        transform.SetParent(another.transform);
                        transform.position = another.transform.position;
                        //transform.localPosition = startPosition;
                        GameManager.instance.UpdateAnsweredQuestions();
                        canMove = false;
                        avaible = false;
                        another.avaible = false;
                        slotReference = another;
                    }else{
                        //transform.position = startPosition.position;
                        transform.localPosition = startPosition;
                    }
                }
                else
                {
                    //transform.position = startPosition.position;
                    transform.localPosition = startPosition;
                }
            }
            else{
                //transform.position = startPosition.position;
                transform.localPosition = startPosition;
            }
            itemImg.raycastTarget = true;
            Debug.Log(name + " OnEndDrag");
        }
    }

    public void OnDrop(PointerEventData data){
        Debug.Log(name + " OnDrop");
    }

    public void OnPointerEnter(PointerEventData pointerEventData){
        Debug.Log(gameObject.name);
    }

    public void OnPointerClick(PointerEventData pointerEventData){
        //Use this to tell when the user right-clicks on the Button
        if (pointerEventData.button == PointerEventData.InputButton.Right) {
            Debug.Log(name + " Game Object Right Clicked!");
        }

        //Use this to tell when the user left-clicks on the Button
        if (pointerEventData.button == PointerEventData.InputButton.Left) {
            Debug.Log(name + " Game Object Left Clicked!");
        }
    }
}
