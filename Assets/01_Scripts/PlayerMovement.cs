using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    
    public float speed;
    private Rigidbody2D myRigidbody;
    private Vector3 change;
    private Animator animator;

    // Start is called before the first frame update
    void Start(){
        animator = GetComponent<Animator>();
        myRigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update(){
        change = Vector3.zero;
        if (!HistoriaCafeteria.dialogoInteractivo)
        {
            change.x = Input.GetAxisRaw("Horizontal");
            change.y = Input.GetAxisRaw("Vertical");
        }
    }

    private void FixedUpdate()
    {
        if (GameManager.instance.gameStarted)
        {
            UpdateAnimationAndMove();
        }
        
        
    }


    void UpdateAnimationAndMove(){
        if (change != Vector3.zero){
            if (change.x > 0)
            {
                transform.localScale = new Vector3(1, 1, 1);
            }
            else
            {
                transform.localScale = new Vector3(-1, 1, 1);
            }
            MoveCharacter();
            animator.SetBool("IsWalking",true);
            //animator.SetFloat("moveY", change.y);
            //animator.SetBool("moving", true);
        }
        else{
            //animator.SetBool("moving", false);
            animator.SetBool("IsWalking", false);
        }
    }

    void MoveCharacter() {
        myRigidbody.MovePosition(
            transform.position + change * speed * Time.fixedDeltaTime
            );
    }
}
