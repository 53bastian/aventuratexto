using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombaClick : MonoBehaviour
{
    public GameObject panelBomba;
    Vector3 bombaStartPoss;
    bool inventoryOpen = true;
    public GameObject inventoryPanel;

    public void OnMouseDown(){
        //panelBomba.SetActive(true);
        HistoriaCafeteria.dialogoInteractivo = true;
        panelBomba.transform.position = bombaStartPoss;
        
        
    }

    public void ExitPanel(){
        //panelBomba.SetActive(false);
        HistoriaCafeteria.dialogoInteractivo = false;
        
        panelBomba.transform.position = new Vector3(3000, 3000, 0);
        
    }

    // Start is called before the first frame update
    void Awake()
    {
        bombaStartPoss = panelBomba.transform.position;
        panelBomba.transform.position = new Vector3(3000, 3000, 0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
